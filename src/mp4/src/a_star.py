import copy
import heapq
import math
import matplotlib.pyplot as plt
import numpy as np
import time


# car state = (x,y)
# state tuple (f,g,(x,y), [(x1,y1),(x2,y2)...])
# total cost f(n) = actual cost g(n) + heuristic cost h(n)
# obstacles = [(x,y), ...]
# min_x, max_x, min_y, max_y are the boundaries of the environment
class a_star:
    def __init__(self, min_x, max_x, min_y, max_y, \
            obstacle=[], resolution=1, robot_size=1):
    ##TODO

    ####

    # state: (total cost f, previous cost g, current position (x,y), \
    # previous motion id, path[(x1,y1),...])
    # start = (sx, sy)
    # end = (gx, gy)
    # sol_path = [(x1,y1),(x2,y2), ...]
    def find_path(self, start, end):
        sol_path = []

        ##TODO

        ####
        return sol_path


def main():
    print(__file__ + " start!!")

    grid_size = 1  # [m]
    robot_size = 1.0  # [m]

    sx, sy = -10, -10
    gx, gy = 10, 10
    obstacle = []
    for i in range(30):
        obstacle.append((i-15, -15))
        obstacle.append((i-14, 15))
        obstacle.append((-15, i-14))
        obstacle.append((15, i-15))

    for i in range(3):
        obstacle.append((0,i))
        obstacle.append((0,-i))

    plt.plot(sx, sy, "xr")
    plt.plot(gx, gy, "xb")
    plt.grid(True)
    plt.axis("equal")

    simple_a_star = a_star(-15, 15, -15, 15, obstacle=obstacle, \
        resolution=grid_size, robot_size=robot_size)
    path = simple_a_star.find_path((sx,sy), (gx,gy))
    print (path)

    rx, ry = [], []
    for node in path:
        rx.append(node[0])
        ry.append(node[1])

    plt.plot(rx, ry, "-r")
    plt.show()


if __name__ == '__main__':
    main()
