"""
Written by Tianqi Liu, 2020 Feb.

It finds the optimal path for a car using Hybrid A* and bicycle model.
"""

import copy
import heapq
import math
import matplotlib.pyplot as plt
import numpy as np
import time


#possible steering controls
possible_str = {
    'l': -10,
    'l+': -50,
    'r+': +50,
    'r': +10,
    's': 0
}

#possible speed controls
possible_sp = {
    'f': 1,
    'b': -1
}


# total cost f(n) = actual cost g(n) + heuristic cost h(n)
class hybrid_a_star:
    def __init__(self, min_x, max_x, min_y, max_y, \
            obstacle=[], resolution=1, vehicle_length=2):
        ##TODO

        ###


    """
    For each node n, we need to store:
    (discret_x, discret_y, heading angle theta),
    (continuous x, continuous y, heading angle theta)
    cost g, f,
    path [(continuous x, continuous y, continuous theta),...]

    start: discret (x, y, theta)
    end: discret (x, y, theta)
    sol_path = [(x1,y1,theta1),(x2,y2,theta2), ...]
    """
    def find_path(self, start, end):
        sol_path = []
        ##TODO

        ###
        return sol_path


def main():
    print(__file__ + " start!!")

    # start and goal position
    #(x, y, theta) in meters, meters, degrees
    sx, sy, stheta= -5, -5, 0
    gx, gy, gtheta = 5, 5, 0

    #create obstacles
    obstacle = []

    for i in range(2):
        obstacle.append((0,i))
        obstacle.append((0,-i))

    ox, oy = [], []
    for (x,y) in obstacle:
        ox.append(x)
        oy.append(y)

    plt.plot(ox, oy, ".k")
    plt.plot(sx, sy, "xr")
    plt.plot(gx, gy, "xb")
    plt.grid(True)
    plt.axis("equal")

    hy_a_star = hybrid_a_star(-6, 6, -6, 6, obstacle=obstacle, \
        resolution=1, vehicle_length=2)
    path = hy_a_star.find_path((sx,sy,stheta), (gx,gy,gtheta))

    rx, ry = [], []
    for node in path:
        rx.append(node[0])
        ry.append(node[1])

    plt.plot(rx, ry, "-r")
    plt.show()


if __name__ == '__main__':
    main()
